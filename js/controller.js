var scheduleApp = angular.module('scheduleApp', ['ui.bootstrap.datetimepicker', 'angularUtils.directives.dirPagination']);

scheduleApp.controller("indexController", function ($scope, $http, $timeout)
{
    moment.locale("ru");

    $scope.settings = {
        handler: "/core/handler.php"
    };

    $scope.dateTimePickerConfig = {
        startView: 'day',
        minView: 'day',
        renderOn: 'courier-changed'
    };

    $scope.regions = [];
    $scope.couriers = [];
    $scope.schedule = [];
    $scope.filteredSchedule = [];

    $scope.filterRange = {};

    $scope.addTripForm = {
        clear: function ()
        {
            $scope.addTripForm.date = null;
        },
        clearErrors: function () {
            for (i in $scope.addTripForm.errors)
            {
                $scope.addTripForm.errors[i] = false;
            }
        },
        hasErrors: function () {
            for (i in $scope.addTripForm.errors)
            {
               if ($scope.addTripForm.errors[i] == true)
                   return true;
            }
            return false;
        },
        errors: {
            badDate: false
        },
        result: false
    };

    $scope.$watch('addTripForm.courier', function (newValue, oldValue)
    {
        $scope.$broadcast('courier-changed');
    });

    $scope.$watch('addTripForm.region', function (newValue, oldValue)
    {
        $scope.$broadcast('courier-changed');
    });

    $scope.$watch('filterRange.dateStart', function (newValue, oldValue)
    {
        $scope.$broadcast('filter-range-start-changed');
        $scope.loadFilteredSchedule();
    });

    $scope.$watch('filterRange.dateEnd', function (newValue, oldValue)
    {
        $scope.$broadcast('filter-range-start-changed');
        $scope.loadFilteredSchedule();
    });

    $scope.loadCouriers = function ()
    {
        $http.get($scope.settings.handler + "?request=getCouriers", {}).then(function (data)
        {
            $scope.couriers = data.data;
        });
    };

    $scope.loadRegions = function ()
    {
        $http.get($scope.settings.handler + "?request=getRegions", {}).then(function (data)
        {
            $scope.regions = data.data;
        });
    };

    $scope.loadSchedule = function ()
    {
        $http.get($scope.settings.handler + "?request=getSchedule", {}).then(function (data)
        {
            $scope.schedule = data.data;
            $scope.$broadcast('courier-changed');
        });
    };

    $scope.getCourierById = function (courierId)
    {
        return $scope.couriers.filter(function (courier)
        {
            return courier.id == courierId;
        })[0];
    };

    $scope.getRegionById = function(regionId)
    {
        return $scope.regions.filter(function (region)
        {
            return region.id == regionId;
        })[0];
    };

    $scope.init = function ()
    {
        $scope.loadRegions();
        $scope.loadCouriers();
        $scope.loadSchedule();
    };

    $scope.beforeTimePickerRender = function ($view, $dates, $leftDate, $upDate, $rightDate) {
        $dates.map(function ($date)
        {
            var currentDate = moment();
            var renderDate = moment(new Date($date.utcDateValue));
            if (renderDate.isBefore(currentDate))
                $date.selectable = false;

            $scope.schedule.map(function (item)
            {
                if (typeof ($scope.addTripForm.courier) != "undefined" && typeof ($scope.addTripForm.region) != "undefined") {
                    if ($scope.addTripForm.courier.id == item.courier_id) {
                        var region = $scope.getRegionById(item.target_region_id);

                        var departureDateStart = moment(new Date(item.departure_date.year, item.departure_date.month - 1, item.departure_date.day, 0, 0));
                        var departureDateEnd = moment(new Date(item.departure_date.year, item.departure_date.month - 1, item.departure_date.day, 0, 0)).add((region.delivery_time * 2), "days");

                        if (renderDate.isBetween(departureDateStart, departureDateEnd)) {
                            $date.selectable = false;
                        }
                    }
                }
            });
        });
    };

    $scope.beforeFilterRangeEndTimePickerRender = function($view, $dates, $leftDate, $upDate, $rightDate)
    {
        if (typeof($scope.filterRange.dateStart) != "undefined")
        {
            var dateStart = moment($scope.filterRange.dateStart);
            $dates.map(function ($date)
            {
                var renderDate = moment(new Date($date.utcDateValue));
                if (renderDate.isBefore(dateStart))
                    $date.selectable = false;
            });
        }
    };

    $scope.loadFilteredSchedule = function ()
    {
        $http.post($scope.settings.handler, {request: 'getSchedule', dateRange: $scope.filterRange}).then(function (data)
        {
            $scope.filteredSchedule = data.data;
        });
    };

    $scope.addTrip = function ()
    {
        $scope.addTripForm.clearErrors();
        var newTripDepartureDateStart = moment($scope.addTripForm.date);
        var newTripDepartureDateEnd = moment($scope.addTripForm.date).add(($scope.addTripForm.region.delivery_time * 2), 'days');
        $scope.schedule.map(function (item) {
            if (item.courier_id == $scope.addTripForm.courier.id) {
                var departureDateStart = moment(new Date(item.departure_date.year, item.departure_date.month - 1, item.departure_date.day, 0, 0));
                if (departureDateStart.isBetween(newTripDepartureDateStart, newTripDepartureDateEnd)) {
                    $scope.addTripForm.errors.badDate = true;
                    return false;
                }
            }
        });

        if ($scope.addTripForm.hasErrors())
            return false;

        $scope.addTripForm.result = false;

        $http.post($scope.settings.handler, {request: 'addTrip', data: $scope.addTripForm}).then(function (data)
        {
            $scope.addTripForm.clear();
            $scope.addTripForm.result = true;

            $timeout(function ()
            {
                $scope.addTripForm.result = false;
            }, 6000);

            $scope.loadSchedule();
            $scope.loadFilteredSchedule();
        });
    }
});