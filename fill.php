<?php
require 'core/classes/Database.php';
require 'core/classes/CouriersCollection.php';
require 'core/classes/RegionsCollection.php';
require 'core/classes/Schedule.php';

$db = Database::getInstance()->db;

$startDate = "2015-06-01 00:00:00";
$lastModifiedData = null;
$endDate = new DateTime("2016-10-31 00:00:00");

$couriers = CouriersCollection::getInstance()->getCouriersList();
$regions = RegionsCollection::getInstance()->getRegionsList();
$schedule = Schedule::getInstance();

foreach ($couriers as $courier)
{
    $lastModifiedData = new DateTime($startDate);

    while ($lastModifiedData < $endDate) {
        $targetRegion = $regions[rand(0, sizeof($regions) - 1)];
        $deliveryTime = $targetRegion["delivery_time"];


        $lastModifiedData->add(new DateInterval("P{$deliveryTime}D"));
        $schedule->addTrip($targetRegion["id"], $courier["id"], $lastModifiedData, false);
        $lastModifiedData->add(new DateInterval("P2D"));
    }
}

?>