<?php

class RegionsCollection
{
    private $db;
    private static $_instance;

    private function __construct()
    {
        $this->db = Database::getInstance()->db;
    }

    private function __clone() {}

    public static function getInstance(): RegionsCollection
    {
        if (self::$_instance === null)
            self::$_instance = new RegionsCollection();
        return self::$_instance;
    }
    
    public function getRegionsList(): array
    {
        $query = $this->db->prepare("SELECT * FROM `regions`");
        $query->execute();
        $data = Database::getArrayFromQuery($query);
        return $data;
    }
}