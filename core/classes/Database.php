<?php

class Database
{
    private static $_instance = null;
    public $db;

    private function __construct()
    {
        $this->db = new PDO("mysql:host=localhost;dbname=test_task", "test_task", "Ja53XHtd4jkBvi0k", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    }

    private function __clone() {}

    public static function getInstance(): Database
    {
        if (self::$_instance === null)
            self::$_instance = new Database();
        return self::$_instance;
    }

    public static function getArrayFromQuery(PDOStatement $data): array
    {
        $tmpData = [];
        while ($row = $data->fetch(PDO::FETCH_ASSOC))
        {
            $tmpData[] = $row;
        }
        return $tmpData;
    }
}

?>