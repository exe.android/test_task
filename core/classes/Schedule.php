<?php

class Schedule
{
    private $db;
    private static $_instance;

    private function __construct()
    {
        $this->db = Database::getInstance()->db;
    }

    private function __clone() {}

    public static function getInstance(): Schedule
    {
        if (self::$_instance === null)
            self::$_instance = new Schedule();
        return self::$_instance;
    }

    public function getSchedule($dateRange = false)
    {
        if (!$dateRange)
            $query = $this->db->prepare("SELECT * FROM `schedule`  ORDER BY `departure_date` ASC");
        else
        {
            $query = $this->db->prepare("SELECT * FROM `schedule` t1 WHERE `departure_date` BETWEEN :startDate AND :endDate  ORDER BY `departure_date` ASC");
            $query->execute([
                "startDate" => $dateRange->dateStart,
                "endDate" => $dateRange->dateEnd
            ]);
        }
        $query->execute();
        $data = Database::getArrayFromQuery($query);
        foreach ($data as $key => $day)
        {
            $date = new DateTime($day["departure_date"]);
            $data[$key]["departure_date"] = [
                "day" => (int) $date->format("d"),
                "month" => (int) $date->format("m"),
                "year" => (int) $date->format("Y")
            ];
        }
        return $data;
    }

    public function addTrip($regionId, $courierId, DateTime $departureDate, $calibrate = true)
    {
        if ($calibrate)
            $departureDate->add(new DateInterval("P1D"));
        $query = $this->db->prepare("INSERT INTO `schedule` (`id`, `target_region_id`, `courier_id`, `departure_date`) VALUES (NULL, :regionId, :courierId, :departureDate)");
        $query->execute([
            "regionId" => $regionId,
            "courierId" => $courierId,
            "departureDate" => $departureDate->format("Y-m-d 00:00:00")
        ]);
    }
}