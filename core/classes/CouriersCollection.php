<?php

class CouriersCollection
{
    private $db;
    private static $_instance;

    private function __construct()
    {
        $this->db = Database::getInstance()->db;
    }

    private function __clone() {}

    public static function getInstance(): CouriersCollection
    {
        if (self::$_instance === null)
            self::$_instance = new CouriersCollection();
        return self::$_instance;
    }

    public function getCouriersList(): array
    {
        $query = $this->db->prepare("SELECT * FROM `couriers`");
        $query->execute();
        $data = Database::getArrayFromQuery($query);
        return $data;
    }

    public function courierIsFreeInDate($courierId, $date): bool
    {
        
    }
}