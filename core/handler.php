<?php
date_default_timezone_set("Europe/Moscow");

require 'classes/Output.php';
require 'classes/RegionsCollection.php';
require 'classes/CouriersCollection.php';
require 'classes/Database.php';
require 'classes/Schedule.php';

if (sizeof($_GET) === 0) {
    $data = file_get_contents("php://input");
    $postData = json_decode($data);
}else{
    $postData = json_decode(json_encode($_GET));
}

if (isset($postData->request))
{
    switch ($postData->request)
    {
        case 'getRegions':
        {
            $regionsCollection = RegionsCollection::getInstance();
            Output::write($regionsCollection->getRegionsList());
            break;   
        }

        case 'getCouriers':
        {
            $couriersCollection = CouriersCollection::getInstance();
            Output::write($couriersCollection->getCouriersList());
            break;
        }

        case 'getSchedule':
        {
            $schedule = Schedule::getInstance();
            if (isset($postData->dateRange))
                Output::write($schedule->getSchedule($postData->dateRange));
            else
                Output::write($schedule->getSchedule());
            break;
        }
        
        case 'addTrip':
        {
            $schedule = Schedule::getInstance();
            $schedule->addTrip($postData->data->region->id, $postData->data->courier->id, new DateTime($postData->data->date));
        }
    }
}

?>